﻿<?php


class Application {
    
    public static $smarty;
    public static $cfg;
    public static $db;
    
    public function run(){
        $this->_init();
        
        try{
            $this->_route();
        } catch (RouteException $e) {
            if (APPLICATION_ENV == 'development'){
                self::$smarty->assign('error_message', $e->getMessage());
            }
            self::$smarty->display('page-404.tpl');
            exit();
        }
    }
    
    protected function _init() {
        defined('APPLICATION_ENV')
                || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

        // Define path to application directory
        defined('APPLICATION_PATH')
                || define('APPLICATION_PATH', realpath(dirname(__FILE__)));
        
        set_include_path(implode(PATH_SEPARATOR, array(
            realpath(APPLICATION_PATH),
            get_include_path(),
        )));
        
        //autoload классов
        function autoload($class_name) {
            $filename = APPLICATION_PATH . '/' . $class_name . '.php';
            if (file_exists($filename))
                require_once($filename);
        }

        spl_autoload_register('autoload');
        
        self::$cfg = parse_ini_file(APPLICATION_PATH . '/configs/general.ini');
        //var_dump($cfg);
        
        self::$db = new PDO('mysql:host='.self::$cfg['dbhost'].';dbname='.self::$cfg['dbbase'], self::$cfg['dbuser'], self::$cfg['dbpass']);
        self::$db->query("SET NAMES 'UTF8'");
        
        //define('SMARTY_SPL_AUTOLOAD', 1);
        require('smarty/Smarty.class.php');
        $smarty = new Smarty;
        $smarty->template_dir = APPLICATION_PATH . '/templates';
        $smarty->compile_dir = APPLICATION_PATH . '/templates/_c';
        self::$smarty = $smarty;
    }
    
    /**
     * Вызывает действия контроллеров.
     * 
     * Если url состоит из двух параметров, разделенных слешами 
     * /contr/someact
     * вызывается ContrController->someactAction();
     * 
     * Если url состоит из одного параметра /contr
     * то вызывается ContrController->indexAction();
     * 
     * Если действие в контроллере не найдено, 
     * /contr/nonexistact
     * то вызывается
     * ContrController->arbitrary('nonexistact');
     * 
     * Если arbitrary не существет у контроллера, то вызывается
     * ContrController->nonexistactAction();
     * 
     * Все вызываемые контроллере должны быть в массиве допустимых.
     * В случае несуществования контроллера или метода, выбрасывает исключение.
     * 
     * @throws RouteException 
     */
    protected function _route(){
        $controller = 'index';
        $action = 'index';
        
        $validControllers = array(
            'index',
            'workers'
        );
        
        $url = self::getUrl();
        if (isset($url[1])){
            if (in_array($url[0], $validControllers)){
                $controller = $url[0];
                $action = $url[1];
            } else {
                throw new RouteException("controller '{$url[0]}' not valid!");
            }
        } else if (isset($url[0]) && $url[0]){
            $controller = $url[0];
            $ctrlName = ucfirst($controller).'Controller';
            if (!class_exists($ctrlName) || !in_array($url[0], $validControllers)) {
                $controller = 'index';
                $action = $url[0];
            }
        }
        
        $ctrlName = ucfirst(strtolower($controller)).'Controller';
        $actionName = $action.'Action';
        
        if (class_exists($ctrlName)) {
            $ctrl = new $ctrlName;
            if (method_exists($ctrl, $actionName)) {
                $ctrl->$actionName();
                echo $ctrl->render();
            } else if (method_exists($ctrl, 'arbitrary')) {
                $urlPart = rtrim($actionName, 'Action');
                $ctrl->arbitrary($urlPart);
                echo $ctrl->render();
            } else {
                throw new RouteException("action '$actionName' is not presented in controller '$ctrlName'");
            }
        } else {
            throw new RouteException("controller class '$ctrlName' not found!");
        }
    }
    
    public static function getUrl() {
        $r_uri = $_SERVER['REQUEST_URI'];
        //убираем из массива параметры после "?"
        $params_pos = mb_strpos($r_uri, '?');
        if ($params_pos) {
            $r_uri = substr($r_uri, 0, $params_pos);
        }
        $r_uri = strtolower(trim($r_uri, '/'));
        $ret = explode('/', $r_uri);
        return $ret;
    }
}

class RouteException extends Exception{}
