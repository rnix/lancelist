
<div class="row-fluid">
    <div class="span3 lspace">
        <ol>
            
        {foreach from=$groups item=grow}

        <li><a href="#gr{$grow.id}">{$grow.title}</a></li>
        {/foreach}
        </ol>
    </div>
    <div class="span9">    
        <table class="table">    
            {foreach from=$groups item=grow}
                    <thead>
                        <tr>
                            <th colspan="2">
                                <h3><a name="gr{$grow.id}">{$grow.title}</a></h3>
                            </th>                    
                        </tr>
                    </thead>            
                    <tbody>
                        {foreach from=$grow.rubrics item=rubrow}
                        <tr>
                            <td>{$rubrow.title}</td>
                            <td>
                            {if isset($search_type) && $search_type=='workers'}
                                <a target="_blank" href="https://twitter.com/#!/search/realtime/{$worker_tag} {$rubrow.search_text}">{$worker_tag} {$rubrow.tags}</a>
                            {else}
                                <a target="_blank" href="https://twitter.com/#!/search/realtime/{$offer_tag|replace:'#':'%23'} {$rubrow.search_text}">{$offer_tag} {$rubrow.tags}</a>
                            {/if}
                            </td>
                        </tr>
                        {/foreach}
                    </tbody>
            {/foreach}
        </table>
    </div>
</div>
