{if isset($mentions) && $mentions}
 
    {foreach from=$mentions item=row key=i}

    <div class="container-fluid twt">
        <div class="row-fluid">
            <div class="span1 best">
                <div>&nbsp;</div>
            </div>
            <div class="span1">                
                <img src="{$row->profile_image_url}" />
            </div>
            <div class="span10">
                <h4><a href="https://twitter.com/#!/{$row->from_user}">@{$row->from_user}</a><span>№{$i} {$row->created_at}</span></h4> 
                <div>{$row->clean_text}</div>  
            </div>
        </div>
    </div>


    {/foreach}
{else}
 no mentions
{/if}