
<h3>Зафолловить проект</h3>
<div class="withb">
  <a href="https://twitter.com/#!/lancelist" target="_blank">Читать @lancelist</a>  
</div>

<h3>Разработчики в Твиттере</h3>
<div class="withb">
	<a href="https://twitter.com/#!/gurovpavel" target="_blank">Читать @gurovpavel</a><br>
	<a href="https://twitter.com/#!/kakov" target="_blank">Читать @kakov</a>
</div>

<h3>Поддержать проект</h3>

<div class="withb">
  {literal}
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="5HLBD7DDJYBX2">
        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
        <img alt="" border="0" src="https://www.paypalobjects.com/ru_RU/i/scr/pixel.gif" width="1" height="1">
    </form>

    <form style="margin: 0; padding: 0;" action="https://money.yandex.ru/charity.xml" method="post">      
      <input type="hidden" name="to" value="41001542597718"/>
      <input type="hidden" name="CompanyName" value="LanceList"/>
      <input type="hidden" name="CompanyLink" value="http://lancelist.pro"/>  
      <input type="submit" value="Отправить" class="btn" />
      <input type="text" style="width: 40px; margin-bottom:0px;" id="CompanySum" name="CompanySum" value="70" /> рублей на счет <strong>41001542597718</strong>
      &nbsp;<font style="color:red">Я</font>ндекс<img src="/img/yandex-money.png" />
    </form>

    {/literal}
</div>      