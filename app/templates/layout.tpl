<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>LanceList - {include file="sitetitles.tpl"}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <style>
      body {
        
      }
    </style>
    

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="/img/favicon.ico">
    
  </head>

  <body>

    <div class="navbar">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
            <a class="logo_brand" href="http://lancelist.pro"><img src="/img/lancelist-logo.png" /></a><a class="brand" href="http://lancelist.pro">LanceList</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="{if $active_page==''}active{/if}"><a href="/"><i class="icon-search"></i> Найти работу</a></li>
              <li class="{if $active_page=='workers'}active{/if}"><a href="/workers"><i class="icon-user"></i> Найти исполнителя</a></li>
              <li class="{if $active_page=='faq'}active{/if}"><a href="/faq"><i class="icon-question-sign"></i> Как это работает?</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">

      {if isset($content)}{$content}{else}no content{/if}
      <hr>
      <footer><a href="https://twitter.com/#!/lancelist" target="_blank">@lancelist</a> - сайт для поиска фриланс-работы через Twitter</footer>
    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script> -->
    
    {literal}
        <!-- Yandex.Metrika counter -->
        <div style="display:none;"><script type="text/javascript">
        (function(w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter12985147 = new Ya.Metrika({id:12985147, enableAll: true});
                }
                catch(e) { }
            });
        })(window, "yandex_metrika_callbacks");
            </script></div>
        <script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript" defer="defer"></script>
        <noscript><div><img src="//mc.yandex.ru/watch/12985147" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
    {/literal}
  </body>
</html>
