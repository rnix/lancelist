<?php


abstract class AbstractController {
    
    public $smarty;
    
    public function __construct() {
        $this->_initAbs();
        $this->_init();
    }
    
    protected function _initAbs(){
        $this->smarty = Application::$smarty;
        $this->smarty->assign('active_page', '');
    }
    
    public function render(){
        return $this->smarty->fetch('layout.tpl');
    }
    
    protected function _init(){
        
    }
}
