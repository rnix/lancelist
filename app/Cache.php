<?php


class Cache {
    
    protected static $_instance;
    protected static $_db;


    public static function getInstance(){
        if (!self::$_instance){
            self::$_instance = new self;
            self::init();
        }
        return self::$_instance;
    }
    
    protected static function init(){
        self::$_db = Application::$db;
    }

    public function set($key, $value) {
        if ($key) {
            $this->clear($key);
            $value = serialize($value);
            $stm = self::$_db->prepare("REPLACE INTO `cache` (`key`, value) VALUES (:key, :value)");
            $stm->execute(array('key'=>$key, 'value'=>$value));
        }
    }
    
    public function get($key, $ttlSeconds){
        if ($key){
            $stm = self::$_db->prepare("SELECT value FROM `cache` WHERE `key`= :key AND update_time > DATE_SUB(NOW(), INTERVAL :ttlSeconds SECOND)");
            $stm->execute(array('key'=>$key, 'ttlSeconds'=>$ttlSeconds));
            $row = $stm->fetch(PDO::FETCH_ASSOC);
            if ($row && isset($row['value'])){
                return unserialize($row['value']);
            }
        }
        return FALSE;
    }
    
    public function clear($key) {
        if ($key) {
            $stm = self::$_db->prepare("DELETE FROM `cache` WHERE `key`=:key");
            return $stm->execute(array('key'=>$key));
        }
    }
       
}
