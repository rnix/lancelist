<?php


class IndexController extends AbstractController{
    
    public function indexAction(){
        $this->workAction();
        //$this->smarty->assign('content', 'bla bla');
    }
    
    public function workAction(){
        $db = Application::$db;
        $stm = $db->prepare("SELECT id, title FROM groups");
        $stm->execute();
        $groupsData = $stm->fetchAll(PDO::FETCH_ASSOC);
        $groups = array();
        foreach ((array)$groupsData as $grow) {
            $stm = $db->prepare("SELECT id, title, gid, search_text, tags FROM rubrics WHERE gid=:gid");
            $stm->execute(array('gid'=>$grow['id']));
            $rubrics = $stm->fetchAll(PDO::FETCH_ASSOC);
            if ($rubrics) {
                $grow['rubrics'] = $rubrics;
                $groups[] = $grow;
            }
        }
        $this->smarty->assign('offer_tag', Application::$cfg['offer_tag']);
        $this->smarty->assign('groups', $groups);
        $cnt = $this->smarty->fetch('work-page.tpl');
        $this->smarty->assign('content', $cnt);
    }
    
        
    public function faqAction(){
        $content = $this->smarty->fetch('faq-page.tpl');
        $this->smarty->assign('content', $content);
        $this->smarty->assign('active_page', 'faq');
    }
    
    public function workersAction(){
        $db = Application::$db;
        $stm = $db->prepare("SELECT id, title FROM groups");
        $stm->execute();
        $groupsData = $stm->fetchAll(PDO::FETCH_ASSOC);
        $groups = array();
        foreach ((array)$groupsData as $grow) {
            $stm = $db->prepare("SELECT id, title, gid, search_text, tags FROM rubrics WHERE gid=:gid");
            $stm->execute(array('gid'=>$grow['id']));
            $rubrics = $stm->fetchAll(PDO::FETCH_ASSOC);
            if ($rubrics) {
                $grow['rubrics'] = $rubrics;
                $groups[] = $grow;
            }
        }
        $this->smarty->assign('worker_tag', Application::$cfg['worker_tag']);
        $this->smarty->assign('search_type', 'workers');
        $this->smarty->assign('groups', $groups);
        $cnt = $this->smarty->fetch('workers-page.tpl');
        $this->smarty->assign('content', $cnt);
        $this->smarty->assign('active_page', 'workers');
    }


    /*
    public function rateAction(){
        $twi = new TwiConnector;
        $res = $twi->getRateLimit();
        var_dump($res);
    }
    */    
}
